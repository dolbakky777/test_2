require "csv"

class FishData
  FRESH_WATER = "freshwater"
  SALT_WATER = "saltwater"
  AVERAGE_LENGTH = "Average length: "
  AVERAGE_WEIGHT = "Average weight: "
  FRESH_WATER_FISH = "Freshwater fish: "
  SALT_WATER_FISH = "Saltwater fish: "
  UNIT_CM = "cm"
  UNIT_KG = "kg"
  NEW_LINE = 'r'
  DELIMITER = "|"

  def initialize
    @array_name = []
    @array_length = []
    @array_weight = []
    @array_fish_type = []
    @sum_length = 0
    @sum_weight = 0
    @fish_type_count = 0  
  end
  
  def output(file_path)
    read_file(file_path)
    puts AVERAGE_LENGTH + average_length.to_s + UNIT_CM
    puts AVERAGE_WEIGHT + average_weight.to_s + UNIT_KG
    puts FRESH_WATER_FISH + fish_type_count(FRESH_WATER).to_s
    puts SALT_WATER_FISH + fish_type_count(SALT_WATER).to_s  
  end

  def read_file(file_path)
    CSV.open(file_path, NEW_LINE) do |colums|
      puts colums.join(DELIMITER)      
      self.name = colums[0]
      self.length = colums[1]
      self.weight = colums[2]
      self.fish_type = colums[3]
    end  
  end

  def name=(s)
    @array_name.push(s)
  end
  def name(s)
    return @array_name[s]
  end
  
  def length=(s)
    @array_length.push(s)
  end
  def length(s)
    return @array_length[s]
  end

  def weight=(s)
    @array_weight.push(s)
  end
  def weight(s)
    return @array_weight[s]
  end
  
  def fish_type=(s)
    @array_fish_type.push(s)
  end
  def fish_type(s)
    return @array_fish_type[s]
  end

  def average_length
    if @array_length.size > 0
      @array_length.inject(0){|@sum_length, num| @sum_length += num.to_i}
      @sum_length / @array_length.size
    else
      0
    end
  end
  
  def average_weight
    if @array_weight.size > 0
      @array_weight.inject(0){|@sum_weight, num| @sum_weight += num.to_i}
      @sum_weight / @array_weight.size
    else
      0
    end
  end

  def fish_type_count(fish_type)
    @fish_type_count = 0
    @array_fish_type.each {|data_fish_type|
      if fish_type == FRESH_WATER
        if data_fish_type == FRESH_WATER
          @fish_type_count += 1
        end
      else
        if data_fish_type == SALT_WATER
          @fish_type_count += 1
        end
      end
    }
    @fish_type_count
  end
end

fish = FishData.new
fish.output(ARGV[0])

